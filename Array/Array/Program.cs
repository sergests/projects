﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] games = { "Alien Shooter", "Tic Tac Toe", "Snake", "Puzzle", "Football" };
            int N;
            Console.ReadLine(Convert.ToInt32(N));

            switch (N)
            {
                case 0:
                        Console.WriteLine("Alien Shooter");
                    break;
                case 1:
                        Console.WriteLine("Tic Tac Toe");
                    break;
                case 2:
                        Console.WriteLine("Snake");
                    break;
                case 3:
                    Console.WriteLine("Puzzle");
                    break;
                case 4:
                    Console.WriteLine("Football");
                    break;
                
                default:
                    Console.WriteLine("Invalid number");
                    break;
            }
            Console.ReadLine();
        }
    }
}
