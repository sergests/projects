﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UdemyCourse
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();

            if (input.Equals("Avetis"))
            {
                Console.WriteLine("Hello Abmin!");
            }
            else if (input.Equals("Joe"))
            {
                Console.WriteLine("Hello Moderator!");
            }
            else 
            {
                Console.WriteLine("Hello Guest!");
            }

        }
    }
}
